let hamburger = document.getElementById("humburger");
let displayMenu = document.getElementById("displayMenu");
let hamburgerImage = document.getElementById("hamburgerImage");

let toggle = false;
function openMenu() {
  if (!toggle) {
    hamburgerImage.src = "images/icon-close.svg";
    // displayMenu.classList.remove("display-menu");
    displayMenu.classList.add("display-menu-card");
    displayMenu.classList.add("display-items");
    toggle = true;
  } else {
    hamburgerImage.src = "images/icon-hamburger.svg";
    displayMenu.classList.add("display-menu");
    displayMenu.classList.remove("display-menu-card");
    displayMenu.classList.remove("display-items");
    toggle = false;
  }
}
